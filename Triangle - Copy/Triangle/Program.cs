﻿using System;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            //=============================
            //Author: Kernen Yabut       
            //Student ID: 8467979
            //Date of Creation: September 26th, 2018    
            //Description: Creates a triangle based on a given amount of stars
            //=============================

            //Ask user for size
            Console.WriteLine("Enter in the number of stars");
            int stars = Convert.ToInt32(Console.ReadLine());

            //Variables
            int preSpace = stars;
            int spaces = 0;

            //Prints each row
            for (int i = 0; i < stars; i++)
            {
                Console.WriteLine("");

                //Counters
                preSpace -= 1;
                spaces += 1;

                //Prints space before the triangle
                for (int k = 0; k < preSpace; k++)
                {
                    Console.Write(" ");
                }

                //Prints elements in each row
                for (int j = 0; j < spaces; j++)
                {
                    //Runs from i to the second last row
                    if (i < stars - 1)
                    {
                        //Prints star if it's the beginning or end of a variable
                        if (j == 0 || i == 0 || i == spaces || j == spaces - 1)
                        {
                            Console.Write("* ");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                    }
                }
            }

            //Prints the last row
            for (int k = 0; k < stars; k++)
            {
                Console.Write("* ");
            }

            Console.ReadLine();
        }
    }
}
