## How to Install

Note: Assuming you have Visual Studio 2017/2019 installed.

1. Download the repository.
2. Extract the repository.
3. Go to the "Triangle - Copy" folder.
4. Double click the "Triangle.sln" file.
5. Wait until Visual Studio is launched.
6. Press the run button on the top.

---

## License

I have chosen an MIT license for this repository as the project in question is mainly academic-focused for beginner programmers. The program itself is en example of loops and input/outputs.
